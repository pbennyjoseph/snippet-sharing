package platform;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name = "SNIPPET")
public class CodeSnippet implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long number;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "UID", unique = true)
    private String id;

    @Column(name = "CODE_SNIPPET", columnDefinition = "TEXT(50000)")
    private String code;

    @Column(name = "LAST_UPDATED")
    private String date;

    @Column(name = "VIEWS_LEFT")
    private long views;

    @Column(name = "TIME_EXPIRY_SEC")
    private long time;

    @Column(name = "RESTRICTED")
    private boolean isRestricted;

    public CodeSnippet() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        this.date = LocalDateTime.now().format(dateTimeFormatter);
//        this.id = UUID.randomUUID().toString();
        this.isRestricted = false;
    }

    public void setId(String id) {
        this.id = id;
    }
    public void setNumber(long number) {
        this.number = number;
    }

    public void setCode(String code) {
        this.code = code;
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        this.date = LocalDateTime.now().format(dateTimeFormatter);
    }

    public void setViews(long views) {
        if (views <= 0 && !isRestricted) {
            views = -1;
        }
        this.views = views;
        this.isRestricted = this.isRestricted || this.views > 0;
    }

    public void setTime(long time) {
        long localTime = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
        if (time <= 0) {
            this.time = 0;
            return;
        }
        this.isRestricted = true;
        this.time = time + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setRestricted(boolean restricted) {
        isRestricted = restricted;
    }


    @JsonIgnore
    public String getId() {
        return id;
    }

    @JsonIgnore
    public long getNumber() {
        return number;
    }

    public String getCode() {
        return code;
    }

    public String getDate() {
        return date;
    }

    public long getViews() {
        return Math.max(views, 0);
    }

    public long getTime() {
        return Math.max(time - LocalDateTime.now().toEpochSecond(ZoneOffset.UTC), 0);
    }

    @JsonIgnore
    public boolean isRestricted() {
        return isRestricted;
    }

    public void setTimeNaive(long time) {
        this.time = time;
    }
}
