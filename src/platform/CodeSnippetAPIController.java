package platform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
public class CodeSnippetAPIController {

    @Autowired
    private ICodeSnippetService codeSnippetService;

    @GetMapping(path = "/api/code/{uid}", produces = "application/json; charset=UTF-8")
    public ResponseEntity<CodeSnippet> getCodeSnippetByIdAPI(@PathVariable String uid) {
        Optional<CodeSnippet> snippet = Optional.ofNullable(codeSnippetService.getSnippetById(uid));
        if (snippet.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Code snippet with id ${uid} was not found.");
        }
        CodeSnippet codeSnippet = snippet.get();
        return new ResponseEntity<>(codeSnippet, HttpStatus.OK);
    }

    @PostMapping(value = "/api/code/new", consumes = "application/json")
    public String addNewCodeSnippet(@RequestBody CodeSnippet codeSnippet) {
        if (codeSnippet.getCode() == null || codeSnippet.getCode().length() == 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Code snippet was empty");
        }
        codeSnippetService.addSnippet(codeSnippet);
        return "{\"id\": \"" + (codeSnippet.getId()) + "\"}";
    }

    @GetMapping(path = "/api/code/latest", produces = "application/json; charset=UTF-8")
    public List<CodeSnippet> getLatestCodeSnippetsAPI() {
        return codeSnippetService.getLatestCodeSnippets();
    }

}
