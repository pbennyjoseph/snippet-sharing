package platform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class CodeSnippetHTMLController {
    @Autowired
    private ICodeSnippetService codeSnippetService;


    @GetMapping(path = "/code/{id}", produces = "text/html")
    public String getCodeSnippetById(@PathVariable String id, Model model) {
        CodeSnippet codeSnippet = codeSnippetService.getSnippetById(id);
        if (codeSnippet == null) {
            return "404";
        }
        model.addAttribute("responseCode", codeSnippet);
        return "code";
    }

    @GetMapping(path = "/code/new", produces = "text/html")
    public String newCode() {
        return "newcode";
    }

    @GetMapping(path = "/code/latest", produces = "text/html")
    public String getLatestCodeSnippets(Model model) {
        List<CodeSnippet> lastTen = codeSnippetService.getLatestCodeSnippets();;
        model.addAttribute("codes", lastTen);
        return "latest";
    }

    @GetMapping(path = "/droptable/{key}", produces = "text/html")
    public String dropTable(@PathVariable String key){
        String sha256hex = org.apache.commons.codec.digest.DigestUtils.sha256Hex(key);
        if ("4658679a5b7682f35e68250a07ff6ff8a745e06a0d755fade0eaa61add084b5f".equals(sha256hex)) {
            codeSnippetService.dropTable();
            return "success";
        } else {
            return "error";
        }
    }

    @GetMapping(path = "/error", produces = "text/html")
    public String getErrorPage() {
        return "error";
    }
}
