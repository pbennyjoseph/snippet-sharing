package platform;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CodeSnippetRepository extends CrudRepository<CodeSnippet, String> {
    @Query(nativeQuery = true, value = "SELECT * FROM SNIPPET" +
            " WHERE RESTRICTED=FALSE" +
            " ORDER BY ID DESC" +
            " LIMIT 10")
    List<CodeSnippet> findLatest();

    @Modifying
    @Query(nativeQuery = true,
            value = "DELETE FROM SNIPPET" +
                    " WHERE RESTRICTED=TRUE AND" +
                    " (VIEWS_LEFT = 0 OR" +
                    " TIME_EXPIRY_SEC <= :time)")
    void removeExpired(@Param("time") long time);

    @Modifying
    @Query(nativeQuery = true, value = "DROP TABLE IF EXISTS SNIPPET;")
    void dropTable();
}
