package platform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CodeSnippetService implements ICodeSnippetService{

    @Autowired
    private CodeSnippetRepository codeSnippetRepository;

    @Override
    public List<CodeSnippet> getAllCodeSnippets() {
        List<CodeSnippet> snippets = new ArrayList<>();
        codeSnippetRepository.findAll().forEach(snippets::add);
        return snippets;
    }

    @Override
    @Transactional
    public CodeSnippet getSnippetById(String id) {
        codeSnippetRepository.removeExpired(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
        Optional<CodeSnippet> snippet = codeSnippetRepository.findById(id);
        if (snippet.isPresent()) {
            CodeSnippet codeSnippet = snippet.get();
            long views = codeSnippet.getViews();
            codeSnippet.setViews(views - 1);
            updateSnippet(codeSnippet);
        }
        return snippet.orElse(null);
    }

    @Override
    public boolean addSnippet(CodeSnippet snippet) {
        if (snippet == null) {
            return false;
        }
        codeSnippetRepository.save(snippet);
        return true;
    }

    @Override
    public void updateSnippet(CodeSnippet snippet) {
        codeSnippetRepository.save(snippet);

    }

    @Override
    public void deleteSnippet(String id) {
        codeSnippetRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void dropTable() {
        codeSnippetRepository.dropTable();
    }

    @Override

    public List<CodeSnippet> getLatestCodeSnippets() {
        return codeSnippetRepository.findLatest();
    }
}
