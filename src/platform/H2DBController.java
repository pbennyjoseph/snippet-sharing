package platform;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class H2DBController {
    static final String JDBC_DRIVER = "org.h2.Driver";
    static final String DB_URL = "jdbc:h2:../snippets";
    static final String USER = "sa";
    static final String PASS = "password";

    private static H2DBController controller = null;

    private Connection connection = null;

    private H2DBController () {
        Statement statement = null;
        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            //STEP 2: Open a connection
            System.out.println("Connecting to database...");
            connection = DriverManager.getConnection(DB_URL,USER,PASS);

            //STEP 3: Execute a query
            System.out.println("Creating table in given database...");
            statement = connection.createStatement();
            String sql =  "CREATE TABLE  IF NOT EXISTS SNIPPETS " +
                    "(ID INTEGER not NULL, " +
                    "UID VARCHAR (40) not NULL, " +
                    "CODE_SNIPPET TEXT, " +
                    "LAST_UPATED TEXT, " +
                    "TIME_EXPIRY_SEC INTEGER not NULL, " +
                    "VIEWS_LEFT INTEGER not NULL, " +
                    "RESTRICTED BOOLEAN not NULL, " +
                    "PRIMARY KEY ( UID ))";
            statement.executeUpdate(sql);
            System.out.println("Created table in given database...");

            // STEP 4: Clean-up environment
            statement.close();
            connection.close();
        } catch(Exception se) {
            se.printStackTrace();
        }
        finally {
            try{
                if(statement !=null) statement.close();
            } catch (Exception ignored) {}
            try {
                if(connection !=null) connection.close();
            } catch(SQLException se){
                se.printStackTrace();
            }
        }
    }

    public static H2DBController getInstance () {
        if (H2DBController.controller == null) {
            H2DBController.controller = new H2DBController();
        }
        return H2DBController.controller;
    }


    public Connection getConnection () {
        return connection;
    }

}
