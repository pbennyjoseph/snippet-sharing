package platform;

import java.util.List;

public interface ICodeSnippetService {
    List<CodeSnippet> getAllCodeSnippets();
    CodeSnippet getSnippetById(String id);
    boolean addSnippet(CodeSnippet snippet);
    void updateSnippet(CodeSnippet snippet);
    void deleteSnippet(String id);
    void dropTable();

    List<CodeSnippet> getLatestCodeSnippets();
}
